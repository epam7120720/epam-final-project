import 'ts-node/register';

import {migrator} from './src/umzug';

(async () => {
    await migrator.runAsCLI();
  })();