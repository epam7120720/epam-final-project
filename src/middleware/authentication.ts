import passport from "passport";
import { Request, Response, NextFunction } from 'express';

/*async function isAuthenticated(req:Request, res:Response, next:NextFunction) {
    passport.authenticate('jwt', { session: true }, (err, user, info) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({ message: 'Authentication failed' });
      }
      req.user = user;
      next();
    })(req, res, next);
  }*/
  const isAuthenticated = passport.authenticate('jwt', { session: false });


  export default isAuthenticated;