import multer from "multer";
import { v4 as uuidv4 } from 'uuid';
const upload = multer({
    limits:{
        fileSize:4 * 1024 * 1024,
    },
    storage:multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'projectimages/'); 
          },
          filename: (req, file, cb) => {
            const uniqueSuffix = uuidv4();
            const ext = file.originalname.split('.').pop();
            const filename = `${file.fieldname}-${uniqueSuffix}.${ext}`;
            cb(null, filename);
          },
    })
});


export default upload;

