import path from "path";

function getImageFullPath(imageName :string="default.png"):string {
    const publicDirectory = path.join(__dirname, '../../public');
    const imagePath = path.join(publicDirectory, imageName);
  
    return imagePath;
  }

  export default getImageFullPath;