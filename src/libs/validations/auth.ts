import { body ,validationResult } from "express-validator";


   const registrationValidation = [
        body('firstName').isString().notEmpty().isLength({max:128}),
        body('lastName').isString().notEmpty().isLength({max:128}),
        body('title').isString().notEmpty().isLength({max:128}),
        body('summary').isString().notEmpty().isLength({max:256}),
        body('email').isEmail().notEmpty(),
        body('password').isLength({ min: 6 }).notEmpty(),
      ];

      const loginValidation = [
        body('email').isEmail().notEmpty(),
        body('password').isLength({ min: 6 }).notEmpty(),
      ];

      export{ registrationValidation , loginValidation}

       