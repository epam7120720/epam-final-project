import pino from "pino";

const logDestination = "logs/app.log";
export const logger = pino(
  {
    prettyPrint: process.env.NODE_ENV === "development",
    serializers: {
      req: (req) => ({
        method: req.method,
        url: req.url,
        correlationId: req.id,
      }),
    },
  },
  pino.destination(logDestination),
);
