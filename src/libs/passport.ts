import passport from "passport";
import { Request, Response, NextFunction } from 'express';
import { Strategy } from 'passport-local'
import { ExtractJwt, Strategy as JwtStrategy, StrategyOptions } from 'passport-jwt';
import { User } from "../models/user.model";

const JWT_SECRET=""
 //Local Strategy
 passport.use(new Strategy({usernameField:'email'}, async(username,password, done)=>{
    try{
        const user = await User.findOne({where:{email:username}});
        if(!user){
            return done(null, false, { message: 'Invalid username or password' });
        }
        const isValid= await user.comparePassword(password);
        if(!isValid){
            return done(null, false, { message: 'Invalid username or password' });
        }
        return done(null,user);
    }catch(err){
        return done(err)
    }
 }));


const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT_SECRET 
};

//JWT strategy
passport.use(new JwtStrategy(jwtOptions, async (jwtPayload, done) => {
  try {
    const user = await User.findByPk(jwtPayload.userId);

    if (!user) {
      return done(null, false, { message: 'User not found' });
    }

    return done(null, user);
  } catch (err) {
    return done(err, false);
  }
}));
 

passport.serializeUser((user:User,done)=>{
    return done(null,user.id)
})

passport.deserializeUser(async(id:number,done)=>{
    try{
        const user= await User.findByPk(id);
        return done(null, user);
    }catch(err){
        return done(err);
    }
})

export default{
    initialize:passport.initialize(),
    session:passport.session(),
    setUser:(req :Request, res:Response, next:NextFunction)=>{
        res.locals.user=req.user;
        return next();
    }
}