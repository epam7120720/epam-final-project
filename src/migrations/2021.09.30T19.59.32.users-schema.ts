import { type MigrationFn } from 'umzug';
import { DataTypes, type Sequelize } from 'sequelize';
import { UserRole } from '../models/user.model';
import bcrypt from 'bcrypt'
import getImageFullPath from '../libs/getFileFullPath';
export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();

  await q.createTable('users', {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    first_name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    last_name: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    },
    image: {
      type: new DataTypes.STRING(256),
      allowNull: false,
      unique:true
    },
    title: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    summary: {
      type: new DataTypes.STRING(256),
      allowNull: false,
    },
    role: {
      type: new DataTypes.STRING(50),
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    created_at: DataTypes.DATE,
    updated_at: DataTypes.DATE,
  });
  const hashed = await bcrypt.hash('pass1234',10)
  await q.bulkInsert('users', [
    {
      first_name: 'John',
      last_name: 'Doe',
      image: getImageFullPath(),
      title: 'User',
      summary: 'A regular user',
      role: UserRole.User,
      email: 'user@gmail.com',
      password: hashed,
      created_at: new Date(),
      updated_at: new Date(),
    },
    {
      first_name: 'Luka',
      last_name: 'Pangani',
      image:  getImageFullPath('admin.png'),
      title: 'Administrator',
      summary: 'An admin user',
      role: UserRole.Admin,
      email: 'admin@gmail.com',
      password: hashed,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();
  q.bulkDelete('Users', { email: ['admin@test.com','user@gmail.com'] }),
  await q.dropTable('users');
};
