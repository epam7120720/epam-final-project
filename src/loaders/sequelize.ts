import { Sequelize } from 'sequelize';
import { type Config } from '../config';

export const loadSequelize = (config: Config): Sequelize => {
  return new Sequelize({
    dialect: 'mysql',
    ...config.db,
  });
};
