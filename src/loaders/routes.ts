import type express from 'express';
import { type Context } from '../interfaces/general';
import { makeAuthRouter } from '../routes/auth';
import { makeExperienceRouter } from '../routes/experience';
import { makeFeedbackRouter } from '../routes/feedback';
import { makeProjectRouter } from '../routes/project';
import { makeUserRouter } from '../routes/user';
export const loadRoutes = (app: express.Router, context: Context): void => {
  app.use('/api/auth', makeAuthRouter(context));
  app.use('api/users',makeUserRouter(context));
  app.use('api/experience', makeExperienceRouter(context));
  app.use('api/feedback', makeFeedbackRouter(context));
  app.use('api/projects', makeProjectRouter(context));
};
