import { Context, type Loader } from '../interfaces/general';
import passport from '../libs/passport';
export const loadPassport: Loader = (app, context) => {
    app.use(passport.initialize);
    app.use(passport.session);
    app.use(passport.setUser);
};
