import { loadMiddlewares } from './middlewares';
import { loadRoutes } from './routes';
import express from 'express';
import { loadContext } from './context';
import { loadModels } from './models';
import { loadSequelize } from './sequelize';
import { config } from '../config';
import { loadPassport } from './passport';
import {logger} from "../libs/logger";
import session from "express-session";
import cookieParser from 'cookie-parser';

export const loadApp = async (): Promise<express.Express> => {
  const app = express();
  
  const sequelize = loadSequelize(config);
  loadModels(sequelize);

  const context = await loadContext();
  
  app.use(
    session({
      name:'session',
      secret:'sdfax1444',
      resave:false,
      saveUninitialized:false,
    })
  )

  app.use(express.urlencoded({extended:true}));
  app.use(express.json());
  app.use(cookieParser());

  app.use((req, res, next) => {
    logger.info("log message for the current request");
    next();
  })

  loadPassport(app, context);
  loadMiddlewares(app, context);
  loadRoutes(app, context);

  return app;
};
