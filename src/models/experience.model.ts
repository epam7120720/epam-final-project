import { DataTypes, Model, type Optional, type Sequelize } from 'sequelize';
import { type Models } from '../interfaces/general';
import { Feedback } from './feedback.model';

interface ExperienceAttributes {
  id: number;
  userId: number;
  companyName: string;
  role: string;
  description: string;
  readonly startDate: Date;
  readonly endDate: Date;
}

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, 'id'>>
  implements ExperienceAttributes
{
  id: number;

  userId: number;

  companyName: string;

  role: string;

  description: string;

  readonly startDate: Date;

  readonly endDate: Date;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
        },
        companyName: {
          field: 'company_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        startDate: {
          type: new DataTypes.DATE(),
          allowNull: false,
        },
        endDate: {
          type: new DataTypes.DATE(),
          allowNull: false,
        },
        description: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'experiences',
        underscored: true,
        sequelize,
      }
    );
  }

  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Feedback.hasMany(models.user, {
      foreignKey: 'user_id',
    });
  }
}
