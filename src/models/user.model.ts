import { DataTypes, Model, type Optional, type Sequelize } from 'sequelize';
import { type Models } from '../interfaces/general';
import bcrypt from 'bcrypt'

export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}

const SALT_ROUNDS =10;

interface UserAttributes {
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;
}

export class User
  extends Model<UserAttributes, Optional<UserAttributes, 'id'>>
  implements UserAttributes
{
  id: number;

  firstName: string;

  lastName: string;

  image: string;

  title: string;

  summary: string;

  role: UserRole;

  email: string;

  password: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  public async comparePassword(candidatePassword: string): Promise<boolean> {
  return bcrypt.compare(candidatePassword, this.password);
  }

  static defineSchema(sequelize: Sequelize): void {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        firstName: {
          field: 'first_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        lastName: {
          field: 'last_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        title: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        summary: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique:true,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        tableName: 'users',
        underscored: true,
        sequelize,
      }
    );
  }
  

  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    User.hasMany(models.project, {
      foreignKey: 'user_id',
    });
    User.hasMany(models.experience, {
      foreignKey: 'user_id',
    });
    User.hasMany(models.feedback, {
      foreignKey: 'from_user',
    });
    User.hasMany(models.feedback, {
      foreignKey: 'to_user',
    });
  }
}
User.beforeSave(async (user, options) => {
  if (user.changed('password')) { 
    try {
      const hash = await bcrypt.hash(user.password, SALT_ROUNDS);
      user.password = hash;
    } catch (err) {
      throw new Error('Error during hashing the password');
    }
  }
});

