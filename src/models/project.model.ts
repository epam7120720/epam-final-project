import { DataTypes, Model, type Optional, type Sequelize } from 'sequelize';
import { type Models } from '../interfaces/general';

interface ProjectAttributes {
  id: number;
  userId: number;
  image: string;
  description: string;
}

export class Project
  extends Model<ProjectAttributes, Optional<ProjectAttributes, 'id'>>
  implements ProjectAttributes
{
  id: number;

  userId: number;

  image: string;

  description: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Project.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
        },
        image: {
          field: 'company_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },

        description: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'projects',
        underscored: true,
        sequelize,
      }
    );
  }

  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Project.belongsTo(models.user, {
      foreignKey: 'user_id',
    });
  }
}
