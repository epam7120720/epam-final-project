import { DataTypes, Model, type Optional, type Sequelize } from 'sequelize';
import { type Models } from '../interfaces/general';

interface FeedbackAttributes {
  id: number;
  fromUserId: number;
  toUserId: number;
  content: string;
  companyName: string;
}

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, 'id'>>
  implements FeedbackAttributes
{
  id: number;

  fromUserId: number;

  toUserId: number;

  userId: number;

  content: string;

  companyName: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUserId: {
          field: 'from_user',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
        },
        toUserId: {
          field: 'to_user',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
        },
        content: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        companyName: {
          field: 'company_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
      },
      {
        tableName: 'feedbacks',
        underscored: true,
        sequelize,
      }
    );
  }

  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Feedback.hasMany(models.user, {
      foreignKey: 'user_id',
    });
  }
}
