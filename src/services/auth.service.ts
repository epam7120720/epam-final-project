import { NextFunction, Request, Response } from "express";
import { body ,validationResult } from "express-validator";
import passport from "passport";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import { User, UserRole } from "../models/user.model";
import getFileFullPath from "../libs/getFileFullPath"


export class AuthService {
    constructor(){};
     

   

   register = async (req: Request, res: Response) => {
    // Check validation errors
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    // registration logic 
    try {
        const { firstName, lastName, title, summary, email, password } = req.body;
        const role =UserRole.User;
        const hashed = await bcrypt.hash(password,10);
      const newUser = await User.create({
            firstName,
            lastName,
            title,
            summary,
            email,
            password:hashed,
            role,
            image:getFileFullPath()
        });

        return res.status(201).json({
            message:'New user was registered',
            user:{
                id:newUser.id,
                firstName:newUser.firstName,
                lastName:newUser.lastName,
                title:newUser.title,
                summary:newUser.summary,
                email:newUser.email,
                image: newUser.image
            }
        })
    } catch (error) {
      return res.status(505).json({ error: 'Registration failed' });
    }
  }

   login = async  (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate('local', (err, user:User, info) => {
        if (err) {
            return res.status(500).json({ error: 'An error occurred' });
        }
        if (!user) {
            return res.status(400).json({ message: info.message });
        }
        req.logIn(user, (err) => {
            if (err) {
                return res.status(500).json({ error: 'An error occurred' });
            }
            const token = jwt.sign({ userId: user.id }, 'your-secret-key', { expiresIn: '1h' });
            return res.status(200).json({ message: 'Login successful',
            user:{
                id: user.id,
                firstName: user.firstName,
                lastName: user.lastName,
                title: user.title,
                summary: user.summary,
                email: user.email,
                image: user.image
            },
             token });
        });
    })(req, res, next);
}
}
