import type express from 'express';
import { type AuthService } from '../services/auth.service';
import { type User } from '../models/user.model';
import { type Project } from '../models/project.model';
import { type Feedback } from '../models/feedback.model';
import { type Experience } from '../models/experience.model';
import { type ExperienceService } from '../services/experience.service';
import { type FeedbackService } from '../services/feedback.service';
import { type ProjectService } from '../services/project.serice';
import { type UserService } from '../services/user.service';
export interface Context {
  services: {
    authService: AuthService;
    experienceService: ExperienceService;
    feedbackService: FeedbackService;
    projectService: ProjectService;
    userService:UserService;
  };
}

export type RouterFactory = (context: Context) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export interface Models {
  user: typeof User;
  project: typeof Project;
  feedback: typeof Feedback;
  experience: typeof Experience;
}
