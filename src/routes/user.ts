import { type Context, type RouterFactory } from '../interfaces/general';
import express, { Response } from 'express';

export const makeUserRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const user =  context.services.userService;

  return router;
};
