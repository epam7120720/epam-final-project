import { type Context, type RouterFactory } from '../interfaces/general';
import express, { Response } from 'express';

export const makeProjectRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const projects =  context.services.projectService;

  return router;
};
