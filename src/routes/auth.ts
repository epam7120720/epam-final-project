import { type Context, type RouterFactory } from '../interfaces/general';
import express from 'express';
import { loginValidation , registrationValidation } from '../libs/validations/auth';
export const makeAuthRouter: RouterFactory = (context: Context) => {
  const router = express.Router();
  const auth =  context.services.authService;

  router.post('/register',registrationValidation ,auth.register);
  router.post('/login', loginValidation , auth.login);

  return router;
};
